<?php

/**
 * @file
 * Definition of Drupal\jt\Tests\JTTest.
 */

namespace Drupal\jt\Tests;

use Drupal\simpletest\WebTestBase;
use Drupal\jt\Transliteration\JTransliteration;

class JTTest extends WebTestBase {

  public static $modules = array('jt');
  
  public static function getInfo() {
    return array(
      'name' => 'Japanese Transliteration',
      'description' => 'Tests Japanese Transliteration base functionality.',
      'group' => 'Transliteration',
    );
  }

  protected $jtInstance;

  protected function setUp() {
    $this->jtInstance = new JTransliteration();
    parent::setUp();
  }

  function testJt() {
    $this->assertEqual(
      $this->jtInstance->kanaToRomaji('アイウエオカキクケコ'),
      'aiueokakikukeko',
      t('Simple standard kana')
    );
    $this->assertEqual(
      $this->jtInstance->kanaToRomaji('アーイーウーエーオーペーパーチーズ'),
      'aaiiuueioupeipaachiizu',
      t('Extended vowels')
    );
    $this->assertEqual(
      $this->jtInstance->kanaToRomaji('ニュースキャストフォラム'),
      'nyuusukyasutoforamu',
      t('Chiisai kana')
    );

    $this->assertEqual(
      $this->jtInstance->kanaToRomaji('カッタケッチティッシュファッション'),
      'kattakecchitisshufasshon',
      t('Chiisai tsu')
    );
  }
}
