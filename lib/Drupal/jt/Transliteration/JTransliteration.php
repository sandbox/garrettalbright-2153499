<?php

/**
 * @file
 * Contains \Drupal\jt\Transliteration\JTransliteration.
 */

namespace Drupal\jt\Transliteration;

use Drupal\Core\Transliteration\PHPTransliteration;

/**
 * The JTransliteration class.
 *
 * @todo Make an interface; make jt_mecab, etc implement a class that extends it.
 */
abstract class JTransliteration extends PHPTransliteration {

  /**
   * Standard kana.
   */
  const KANA_STANDARD = 0;
  
  /**
   * "Chiisai" kana variants that clobber the vowel of previous kana.
   */
  const KANA_CHIISAI = 1;
  
  /**
   * The chiisai tsu.
   */
  const KANA_CHIISAI_TSU = 2;
  
  /**
   * The vowel extension character.
   */
  const KANA_LONG_VOWEL = 3;

  /**
   * Kana with a multi-character consonant part; eg SHi, TSu, CHi.
   */
  const KANA_MULTI_CHAR_CONS = 4;

  /**
   * Punctuation.
   */
  const PUNC = 5;

  /**
   * Convert Japanese text to katakana. Non-Japanese text should be left as is.
   *
   * @param $text
   *   Text that will contain Japanese text.
   * @return string
   *   Text with any Japanese text converted to katakana.
   */
  abstract public function toKana($text);

  /**
   * Romanize all-katakana text.
   *
   * We are mostly using the Hepburn method of romanization, with a few
   * exceptions:
   * - Long vowels are extended with a vowel instead of the non-ASCII-
   *   friendly macron (overlined) vowel; eg, ローマジ becomes "roumaji" instead of
   *   "rōmaji"
   * - Non-terminal ン characters (eg, as in シンパイ) are transliterated to just "n"
   *   instead of "m" or "n'"
   *
   * The script does its best to handle non-standard but playful usage like
   * 「ゴオオオオォォォ！」or「ヤッッッター！」with usable results.
   *
   * @todo Should we "fix" that second point of non-Hepburn romanization above?
   *
   * @param $text
   *   Text to transliterate. The text should be entirely katakana (or at least
   *   the Japanese parts of it).
   * @return string
   *   The transliterated text.
   */
  public function kanaToRomaji($text) {
    $kana_data = $this->kanaData();
    // Store data for kana in the text
    $text_kana = array();
    // Count how many times we should repeat kana
    $c_repeat_count = 0;
    // Loop through the kana
    for ($char_pos = 0; $char_pos < drupal_strlen($text); $char_pos++) {
      $char = drupal_substr($text, $char_pos, 1);
      if (!isset($kana_data[$char])) {
        // If we don't have kana data, just pass the character through.
        $text_kana[] = $char;
      }
      else {
        // Get data for the kana
        $kana = $kana_data[$char];
          // Handle chiisai kana variants
        if ($kana['type'] === self::KANA_CHIISAI) {
          // Examine the previous kana
          $prev = $char_pos - 1;
          if (isset($text_kana[$prev])) {
            // Clobber the vowel of the previous kana
            $text_kana[$prev]['v'] = '';
            // Make sure シャ／チュ become sha/chu instead of shya/tsyu
            if ($text_kana[$prev]['type'] === self::KANA_MULTI_CHAR_CONS) {
              // Clobber the "y"
              $kana['c'] = '';
            }
          }
        }
        elseif ($kana['type'] === self::KANA_LONG_VOWEL) {
          // Examine the previous kana
          $prev = $char_pos - 1;
          // Assign a vowel to this kana based on the vowel of the previous one.
          if (isset($text_kana[$prev])) {
            switch ($text_kana[$prev]['v']) {
              case 'e':
                $kana['v'] = 'i';
                break;
              case 'o':
                $kana['v'] = 'u';
                break;
              case '':
                break;
              default: // a, i, u
                $kana['v'] = $text_kana[$prev]['v'];
            }
          }
        }
        else {
          if ($kana['type'] === self::KANA_CHIISAI_TSU) {
            // Note that we want to repeat the consonant again.
            $c_repeat_count++;
          }
          elseif ($c_repeat_count > 0) {
            // Repeat the first character of the consonant part of the kana.
            // We don't repeat the full consonant part because 「ケッチ」 would
            // become "kechchi" instead of the desired "kecchi."
            $kana['c'] = str_repeat($kana['c']{0}, $c_repeat_count) . $kana['c'];
            $c_repeat_count = 0;
          }
        }
        $text_kana[] = $kana;
      }
    }
    // Now do the actual building of the resultant string.
    $transliterated = '';
    foreach ($text_kana as $pos => $kana) {
      if (is_string($kana)) {
        // If it's just a string, we just passed through the character (we
        // didn't have data for it).
        $transliterated .= $kana;
      }
      else {
        // Append the consonant part followed by the vowel part.
        $transliterated .= $kana['c'] . $kana['v'];
      }
    }
    return $transliterated;
  }
  
  /**
   * Store data on katakana and how they are transliterated.
   *
   * I'm not crazy about one-off data storage functions like this, but I haven't
   * yet thought of a better way to store this sort of data…
   *
   * @return array
   *   An array, keyed by a katakana character, with the following values:
   *   - c: A transliteration of the kana's consonant sound
   *   - v: A transliteration of the kana's vowel sound
   *   - type: A constant with information about the type of the kana
   */
  protected function kanaData() {
    return array(
      // In Unicode character order
      // x301a
      'ァ' => array(
        'c' => '',
        'v' => 'a',
        'type' => self::KANA_CHIISAI,
      ),
      
      'ア' => array( 
        'c' => '',
        'v' => 'a',
        'type' => self::KANA_STANDARD,
      ),
      'ィ' => array( 
        'c' => '',
        'v' => 'i',
        'type' => self::KANA_CHIISAI,
      ),
      'イ' => array( 
        'c' => '',
        'v' => 'i',
        'type' => self::KANA_STANDARD,
      ),
      'ゥ' => array( 
        'c' => '',
        'v' => 'u',
        'type' => self::KANA_CHIISAI,
      ),
      'ウ' => array(
        'c' => '',
        'v' => 'u',
        'type' => self::KANA_STANDARD,
      ),
      'ェ' => array(
        'c' => '',
        'v' => 'e',
        'type' => self::KANA_CHIISAI,
      ),
      'エ' => array(
        'c' => '',
        'v' => 'e',
        'type' => self::KANA_STANDARD,
      ),
      'ォ' => array(
        'c' => '',
        'v' => 'o',
        'type' => self::KANA_CHIISAI,
      ),
      'オ' => array(
        'c' => '',
        'v' => 'o',
        'type' => self::KANA_STANDARD,
      ),
      // x30ab
      'カ' => array(
        'c' => 'k',
        'v' => 'a',
        'type' => self::KANA_STANDARD,
      ),
      'ガ' => array(
        'c' => 'g',
        'v' => 'a',
        'type' => self::KANA_STANDARD,
      ),
      'キ' => array(
        'c' => 'k',
        'v' => 'i',
        'type' => self::KANA_STANDARD,
      ),
      'ギ' => array(
        'c' => 'g',
        'v' => 'i',
        'type' => self::KANA_STANDARD,
      ),
      'ク' => array(
        'c' => 'k',
        'v' => 'u',
        'type' => self::KANA_STANDARD,
      ),
      'グ' => array(
        'c' => 'g',
        'v' => 'u',
        'type' => self::KANA_STANDARD,
      ),
      'ケ' => array(
        'c' => 'k',
        'v' => 'e',
        'type' => self::KANA_STANDARD,
      ),
      'ゲ' => array(
        'c' => 'g',
        'v' => 'e',
        'type' => self::KANA_STANDARD,
      ),
      'コ' => array(
        'c' => 'k',
        'v' => 'o',
        'type' => self::KANA_STANDARD,
      ),
      'ゴ' => array(
        'c' => 'g',
        'v' => 'o',
        'type' => self::KANA_STANDARD,
      ),
      // x30b5
      'サ' => array(
        'c' => 's',
        'v' => 'a',
        'type' => self::KANA_STANDARD,
      ),
      'ザ' => array(
        'c' => 'z',
        'v' => 'a',
        'type' => self::KANA_STANDARD,
      ),
      'シ' => array(
        'c' => 'sh',
        'v' => 'i',
        'type' => self::KANA_MULTI_CHAR_CONS,
      ),
      'ジ' => array(
        'c' => 'j',
        'v' => 'i',
        'type' => self::KANA_STANDARD,
      ),
      'ス' => array(
        'c' => 's',
        'v' => 'u',
        'type' => self::KANA_STANDARD,
      ),
      'ズ' => array(
        'c' => 'z',
        'v' => 'u',
        'type' => self::KANA_STANDARD,
      ),
      'セ' => array(
        'c' => 's',
        'v' => 'e',
        'type' => self::KANA_STANDARD,
      ),
      'ゼ' => array(
        'c' => 'z',
        'v' => 'e',
        'type' => self::KANA_STANDARD,
      ),
      'ソ' => array(
        'c' => 's',
        'v' => 'o',
        'type' => self::KANA_STANDARD,
      ),
      'ゾ' => array(
        'c' => 'z',
        'v' => 'o',
        'type' => self::KANA_STANDARD,
      ),
      // 30bf
      'タ' => array(
        'c' => 't',
        'v' => 'a',
        'type' => self::KANA_STANDARD,
      ),
      'ダ' => array(
        'c' => 'd',
        'v' => 'a',
        'type' => self::KANA_STANDARD,
      ),
      'チ' => array(
        'c' => 'ch',
        'v' => 'i',
        'type' => self::KANA_MULTI_CHAR_CONS,
      ),
      'ヂ' => array(
        'c' => 'j',
        'v' => 'i',
        'type' => self::KANA_STANDARD,
      ),
      'ッ' => array(
        'c' => '',
        'v' => '',
        'type' => self::KANA_CHIISAI_TSU
      ),
      'ツ' => array(
        'c' => 'ts',
        'v' => 'u',
        'type' => self::KANA_MULTI_CHAR_CONS,
      ),
      'ヅ' => array(
        'c' => 'd',
        'v' => 'u',
        'type' => self::KANA_STANDARD,
      ),
      'テ' => array(
        'c' => 't',
        'v' => 'e',
        'type' => self::KANA_STANDARD,
      ),
      'デ' => array(
        'c' => 'd',
        'v' => 'e',
        'type' => self::KANA_STANDARD,
      ),
      'ト' => array(
        'c' => 't',
        'v' => 'o',
        'type' => self::KANA_STANDARD,
      ),
      'ド' => array(
        'c' => 'd',
        'v' => 'o',
        'type' => self::KANA_STANDARD,
      ),
      // 30ca
      'ナ' => array(
        'c' => 'n',
        'v' => 'a',
        'type' => self::KANA_STANDARD,
      ),
      'ニ' => array(
        'c' => 'n',
        'v' => 'i',
        'type' => self::KANA_STANDARD,
      ),
      'ヌ' => array(
        'c' => 'n',
        'v' => 'u',
        'type' => self::KANA_STANDARD,
      ),
      'ネ' => array(
        'c' => 'n',
        'v' => 'e',
        'type' => self::KANA_STANDARD,
      ),
      'ノ' => array(
        'c' => 'n',
        'v' => 'o',
        'type' => self::KANA_STANDARD,
      ),
      // 30cf
      'ハ' => array(
        'c' => 'h',
        'v' => 'a',
        'type' => self::KANA_STANDARD,
      ),
      'バ' => array(
        'c' => 'b',
        'v' => 'a',
        'type' => self::KANA_STANDARD,
      ),
      'パ' => array(
        'c' => 'p',
        'v' => 'a',
        'type' => self::KANA_STANDARD,
      ),
      'ヒ' => array(
        'c' => 'h',
        'v' => 'i',
        'type' => self::KANA_STANDARD,
      ),
      'ビ' => array(
        'c' => 'b',
        'v' => 'i',
        'type' => self::KANA_STANDARD,
      ),
      'ピ' => array(
        'c' => 'p',
        'v' => 'i',
        'type' => self::KANA_STANDARD,
      ),
      'フ' => array(
        'c' => 'f',
        'v' => 'u',
        'type' => self::KANA_STANDARD,
      ),
      'ブ' => array(
        'c' => 'b',
        'v' => 'u',
        'type' => self::KANA_STANDARD,
      ),
      'プ' => array(
        'c' => 'p',
        'v' => 'u',
        'type' => self::KANA_STANDARD,
      ),
      'ヘ' => array(
        'c' => 'h',
        'v' => 'e',
        'type' => self::KANA_STANDARD,
      ),
      'ベ' => array(
        'c' => 'b',
        'v' => 'e',
        'type' => self::KANA_STANDARD,
      ),
      'ペ' => array(
        'c' => 'p',
        'v' => 'e',
        'type' => self::KANA_STANDARD,
      ),
      'ホ' => array(
        'c' => 'h',
        'v' => 'o',
        'type' => self::KANA_STANDARD,
      ),
      'ボ' => array(
        'c' => 'b',
        'v' => 'o',
        'type' => self::KANA_STANDARD,
      ),
      'ポ' => array(
        'c' => 'p',
        'v' => 'o',
        'type' => self::KANA_STANDARD,
      ),
      // 30de
      'マ' => array(
        'c' => 'm',
        'v' => 'a',
        'type' => self::KANA_STANDARD,
      ),
      'ミ' => array(
        'c' => 'm',
        'v' => 'i',
        'type' => self::KANA_STANDARD,
      ),
      'ム' => array(
        'c' => 'm',
        'v' => 'u',
        'type' => self::KANA_STANDARD,
      ),
      'メ' => array(
        'c' => 'm',
        'v' => 'e',
        'type' => self::KANA_STANDARD,
      ),
      'モ' => array(
        'c' => 'm',
        'v' => 'o',
        'type' => self::KANA_STANDARD,
      ),
      // 30e3
      'ャ' => array(
        'c' => 'y',
        'v' => 'a',
        'type' => self::KANA_CHIISAI,
      ),
      'ヤ' => array(
        'c' => 'y',
        'v' => 'a',
        'type' => self::KANA_STANDARD,
      ),
      'ュ' => array(
        'c' => 'y',
        'v' => 'u',
        'type' => self::KANA_CHIISAI,
      ),
      'ユ' => array(
        'c' => 'y',
        'v' => 'u',
        'type' => self::KANA_STANDARD,
      ),
      'ョ' => array(
        'c' => 'y',
        'v' => 'o',
        'type' => self::KANA_CHIISAI,
      ),
      'ヨ' => array(
        'c' => 'y',
        'v' => 'o',
        'type' => self::KANA_STANDARD,
      ),
      // 30e9
      'ラ' => array(
        'c' => 'r',
        'v' => 'a',
        'type' => self::KANA_STANDARD,
      ),
      'リ' => array(
        'c' => 'r',
        'v' => 'i',
        'type' => self::KANA_STANDARD,
      ),
      'ル' => array(
        'c' => 'r',
        'v' => 'u',
        'type' => self::KANA_STANDARD,
      ),
      'レ' => array(
        'c' => 'r',
        'v' => 'e',
        'type' => self::KANA_STANDARD,
      ),
      'ロ' => array(
        'c' => 'r',
        'v' => 'o',
        'type' => self::KANA_STANDARD,
      ),
      //30ee - rare
      'ヮ' => array(
        'c' => 'w',
        'v' => 'a',
        'type' => self::KANA_CHIISAI,
      ),
      'ワ' => array(
        'c' => 'w',
        'v' => 'a',
        'type' => self::KANA_STANDARD,
      ),
      // Rare
      'ヰ' => array(
        'c' => 'w',
        'v' => 'i',
        'type' => self::KANA_STANDARD,
      ),
      // Rare
      'ヱ' => array(
        'c' => 'w',
        'v' => 'e',
        'type' => self::KANA_STANDARD,
      ),
      'ン' => array(
        'c' => 'n',
        'v' => '',
        'type' => self::KANA_STANDARD,
      ),
      // 30f4 - All following kana are rare
      'ヴ' => array(
        'c' => 'v',
        'v' => 'u',
        'type' => self::KANA_STANDARD,
      ),
      // Skipping ヵ and ヶ since they're more like kanji…
      'ヷ' => array(
        'c' => 'v',
        'v' => 'a',
        'type' => self::KANA_STANDARD,
      ),
      'ヸ' => array(
        'c' => 'v',
        'v' => 'i',
        'type' => self::KANA_STANDARD,
      ),
      'ヹ' => array(
        'c' => 'v',
        'v' => 'e',
        'type' => self::KANA_STANDARD,
      ),
      'ヺ' => array(
        'c' => 'v',
        'v' => 'o',
        'type' => self::KANA_STANDARD,
      ),
      '・' => array(
        'c' => '_',
        'v' => '',
        'type' => self::PUNC,
      ),
      'ー' => array(
        'c' => '',
        'v' => '',
        'type' => self::KANA_LONG_VOWEL,
      ),
    );
  }


}
