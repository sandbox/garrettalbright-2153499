<?php

/**
 * @file
 * Contains \Drupal\jt_mecab\Transliteration\JTransliterationMeCab.
 */

namespace Drupal\jt_mecab\Transliteration;

use Drupal\jt\Transliteration\JTransliteration;

/**
 * The JTransliterationMeCab class.
 */
class JTransliterationMeCab extends JTransliteration {

  /**
   * {@inheritdoc}
   */
  public function transliterate($string, $langcode = 'en', $unknown_character = '?', $max_length = NULL) {
    if ($langcode === 'ja') {
      $string = $this->kanaToRomaji($this->toKana($string));
    }
    // Allow transliteration of non-Japanese characters
    return parent::transliterate($string, $langcode, $unknown_character, $max_length);
  }

  /**
   * {@inheritdoc}
   */
  public function toKana($text) {
    $mecab = new \Mecab_Tagger();
    $parsed = $mecab->parse($text);
    // From both the command line and the PHP interfaces, attempts to format the
    // output of the data fail. So we'll just parse what Mecab gives us.
    // @todo Is there a better way to do this?
    $kana = '';
    preg_match_all('/^([^\t]+)\t([^\t]+)\t([^\t]+)/m', $parsed, $matches, PREG_SET_ORDER);
    foreach ($matches as $match) {
      // If the first field is valid output, pass it through. This is because
      // Mecab will spell out digits (turn "1" into "イチ" and so on) when it would
      // be better to just keep the digits.
      $kana .= preg_match('/[^abcdefghijklmnopqrstuvwxyz0-9_]/', $match[1]) ? $match[3] : $match[1];
    }
    return $kana;
  }

}
